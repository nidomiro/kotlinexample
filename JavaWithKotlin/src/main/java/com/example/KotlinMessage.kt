package com.example

class KotlinMessage {

    companion object {
        const val greeting = "KotlinMessage: Hello World."

        fun greet(){
            println("greet(): $greeting")
        }

        @JvmStatic
        fun greetJvmStatic(){
            println("greetJvmStatic(): $greeting")
        }
    }
}