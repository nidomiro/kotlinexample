package com.example;

public class JavaMessage {
    public static final String greeting = "JavaMessage: Hello World.";

    public static void greet() {
        System.out.println(greeting);
    }
}
