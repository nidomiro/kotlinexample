package com.example

import com.example.dto.JavaPersonDto
import com.example.dto.KotlinPersonDto


fun main(args: Array<String>) {

    println(JavaMessage.greeting)
    JavaMessage.greet()

    printSeperator()

    println(KotlinMessage.greeting)
    KotlinMessage.greet()
    KotlinMessage.greetJvmStatic()

    printSeperator()

    val javaPersonDto = JavaPersonDto(null, "Hans", "Vader", "hans@vader.imperial")
    println(javaPersonDto)

    printSeperator()

    val kotlinPersonDto = KotlinPersonDto(emailAddress = "hans@vader.imperial", firstName = "Hans", name = "Vader")
    println(kotlinPersonDto)

    printSeperator()

    val (identifier, firstName, name, emailAddress) = kotlinPersonDto
    println(identifier)
    println(firstName)
    println(name)
    println(emailAddress)

    printSeperator()

    val kotlinPersonDtoCopy = kotlinPersonDto.copy(identifier = 5)
    println(kotlinPersonDtoCopy)

    printSeperator()



}


fun printSeperator(seperator: String="-") {
    println("\n${seperator.repeat(20)}\n")
}