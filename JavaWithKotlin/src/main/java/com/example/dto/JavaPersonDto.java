package com.example.dto;

import java.util.Objects;

public class JavaPersonDto {

    private final Long id;

    private final String firstName;

    private final String name;

    private final String emailAddress;

    public JavaPersonDto(Long id, String firstName, String name, String emailAddress) {
        assert firstName != null;
        assert name != null;
        assert emailAddress != null;

        this.id = id;
        this.firstName = firstName;
        this.name = name;
        this.emailAddress = emailAddress;
    }

    public String getDisplayName(){
        return name + ", " + firstName;
    }

    public Long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getName() {
        return name;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        JavaPersonDto that = (JavaPersonDto) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(firstName, that.firstName) &&
                Objects.equals(name, that.name) &&
                Objects.equals(emailAddress, that.emailAddress);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, firstName, name, emailAddress);
    }

    @Override
    public String toString() {
        return "JavaPersonDto{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", name='" + name + '\'' +
                ", emailAddress='" + emailAddress + '\'' +
                '}';
    }
}
