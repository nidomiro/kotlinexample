package com.example.dto

data class KotlinPersonDto(
        val identifier: Long? = null,
        val firstName: String,
        val name: String,
        val emailAddress: String

){
    val displayName get() = "$name, $firstName"
}