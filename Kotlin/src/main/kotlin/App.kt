import java.math.BigDecimal

fun main(args: Array<String>) {

    valVsVar()

    nullSafety()

    smartCasts()

    operatorOverloading()

    infix()

    delegates()

    tailRecursion()

    extensionFunctions()

}


fun valVsVar() {
    println("\n# valVsVar:")
    val notEditable = 1

    var editable = 1
    editable = 2

    println(notEditable)
    println(editable)
}


fun nullSafety() {
    println("\n# nullSafety:")

    val notNullString = "NotNull"
    println(notNullString.repeat(3))

    val nullableString: String? = notNullString
    println(nullableString?.repeat(3) ?: "Null")

}


fun smartCasts() {
    println("\n# smartCasts:")

    val n: Number = 5

    when (n) {
        is Double -> println("n is a Double")
        is Int -> println("n is a Int")
        else -> println("n is o unknown tyle ${n::class.simpleName}")
    }
}


private fun operatorOverloading() {
    println("\n# operatorOverloading:")

    println(BigDecimal.valueOf(5.5) + BigDecimal.valueOf(5))
}


private fun infix() {
    println("\n# infix:")

    val robot = Robot("Robo")

    robot move Direction.RIGHT
    robot move Direction.FORWARD


    for (i in 0 until 20 step 2) {
        print("$i ")
    }
    println()

    for (i in 10 downTo 1) {
        print("$i ")
    }
    println()

}


fun delegates(){
    println("\n# delegates:")

    val heavyCalculation: Int by lazy {
        println("evaluating lazy")
        (1..100).sum()
    }

    val b = true

    if (b) {
        if (heavyCalculation < 100){
            println("less than 100")
        }
        println("it is $heavyCalculation")
    }
}


private fun tailRecursion() {
    println("\n# tailRecursion:")

    println(factorial(4))

}
private fun factorial(n: Int): Int {
    tailrec fun internalFactorial(n: Int, sum: Int = 1): Int =
            if (n == 1) sum else internalFactorial(n - 1, sum * n)

    return internalFactorial(n)
}


private fun extensionFunctions() {
    println("\n# extensionFunctions:")

    val str = "My awesome string"
    val strAsUtf8Array = str.utf8Array()
            .joinToString(
                    separator = ", "
                    , transform = { String.format("0x%02X", it)}
            )

    println("'$str' -> $strAsUtf8Array")

}
fun String.utf8Array(): ByteArray = this.toByteArray(Charsets.UTF_8)

