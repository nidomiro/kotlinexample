


class Robot(
        private val name: String
) {

    infix fun move(direction: Direction){
        println("Robot '$name' moves $direction")
    }

}

enum class Direction {
    FORWARD,
    BACKWARD,
    LEFT,
    RIGHT
}