# Kotlin Beispiele

Kotlin ist ein Programmiersprache die ursprünglich für die JVM von JetBrains entwickelt wurde.
Mittlerweile gibt es zusätzlich auch Kotlin/JS und Kotlin/Native.

Kotlins Null-Behandlung wird extrem oft als sehr großer Vorteil in diversen Artikeln zitiert.
Kotlin zwingt den Programmierer sich mit möglichen Null-Werten auseinander zu setzten,
wobei er jedoch von einer passenden Syntax unterstützt wird.

Kotlin kann mittels eines J2K-Compilers aus Java-Code generiert werden.
Es ist jedoch auch möglich Kotlin und Java nebeneinander zu verwenden.
Diese Interkompatibilität von Kotlin und Java macht es sehr einfach bestehende Java-Applikationen auf Kotlin zu portieren.


Auch wenn Kotlin viele Vorteile besitzt können z.B. [Extension-Functions nicht dazu verwendet werden Statische Methoden in bestehenden Java klassen hinzuzufügen.](https://stackoverflow.com/q/33911457/1469540)

## Links
* Kotlin Lernen: [https://try.kotlinlang.org/#/Kotlin%20Koans/Introduction/Hello,%20world!/Task.kt](https://try.kotlinlang.org/#/Kotlin%20Koans/Introduction/Hello,%20world!/Task.kt)

* Beispiel Repository : [https://gitlab.com/nidomiro/kotlinexample](https://gitlab.com/nidomiro/kotlinexample)

* Beispiel Spring-Boot Anwendung (noch in der Entwicklung):  [https://gitlab.com/nidomiro.de/sportpoint](https://gitlab.com/nidomiro.de/sportpoint)

## Merksätze
* Nullpointerbehandlung per Compiler ist nett, kann aber auch zur Bürde werden -> DTOs
* Generics können durch die genauere Evaluierung  schwieriger werden. (Wenn man komplizierte Dinge tun will)
